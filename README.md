# Using Kibana to show metricbeat and user logging 

Prerequisites :
```sh
Docker Desktop
MetricBeat
```


How to start Wordpress : 

- clone our project in `https://gitlab.com/joshuanurdi/wordpress-docker`
- go to scripts directory, and find " .env.tmpl ". copy the file, and paste it in the same directory as docker-compose.yml
- rename the file to  .env
- open command prompt / powershell / command line
- go into this project directory in your terminal.
- run command `docker-compose up -d` (you need to install docker-compose before running this.) 
[`click here to install docker compose`](https://docs.docker.com/compose/install/)
- to open wordpress, run " localhost " in your browser. 


How to start kibana :
- Make sure your wordpress is already running in your localhost.
- Run " localhost:9200 " to check elasticsearch connection.
- to open kibana, run " localhost:5601 " in your browser.
 
How to start metricbeat for windows :
- [`Download metricbeat on your computer`](https://www.elastic.co/downloads/past-releases/metricbeat-7-6-1)
- Open powershell and run as Administrator.
- Change directory to "C:\Program Files\Metricbeat" and run command `.\install-service-metricbeat.ps1`.
- Modify metricbeat.yml to point to your Elasticsearch installation.
    ```sh
    output.elasticsearch:
      hosts: ["<es_url>"]
      username: "elastic"
      password: "<password>"
    setup.kibana:
      host: "<kibana_url>"
    ```
- run command `.\metricbeat.exe` modules enable docker in powershell.
- Modify docker.yml in folder modules.d
    ```sh
    - module: docker
  metricsets:
   - container
   - cpu
   - diskio
   - event
   - healthcheck
   - info
   - memory
   - network
  period: 10s
  hosts: ["npipe:////./pipe/docker_engine"]
  enabled: true
  ```
- run command `.\metricbeat.exe setup`
( Ensure that kibana is running and reachable in your localhost before running this command )
- run command `.\metricbeat.exe -e -d "*"`
- Check metric data of your container in kibana dashboard.


How to start fluentd :
- Open kibana, and go to management menu. 
- Add fluentd to your index pattern.
- go to visualize menu, and save the fluentd visualization.
- go to dashboard menu, then add the fuentd visualization that we saved earlier. (try using another browser if dashboard did not load )
- User activity logs will be displayed in the kibana dashboard.

                                                      Thankyou.


